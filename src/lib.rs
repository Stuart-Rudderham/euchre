extern crate itertools;
extern crate rand;

pub mod card;
pub mod player;
pub mod euchre;
pub mod logging;
