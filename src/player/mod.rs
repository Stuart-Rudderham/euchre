#![allow(non_snake_case)]

use crate::card::*;
use crate::euchre;
use crate::euchre::utils::*;

pub mod brain;
use crate::player::brain::*;

pub struct Player<'a>
{
    pub hand  : MutableCardHand,
    pub index : PlayerIndex,
    pub brain : &'a mut dyn IPlayerBrain,
}

impl<'a> Player<'a>
{
    pub fn New(aBrain : &'a mut dyn IPlayerBrain, aHand : CardHand, anIndex : PlayerIndex) -> Player<'a>
    {
        assert!( IsValidPlayerIndex(anIndex) );

        Player
        {
            hand  : CreateMutableCardHand(aHand)    ,
            index : anIndex                         ,
            brain : aBrain                          ,
        }
    }

    pub fn CanPickupCardFromKitty(&self, cardFromKitty : &Card) -> bool
    {
        let trumpSuit = None; // Since we're in the process of choosing it.

        DoesHandHaveSuitInIt(&self.hand, cardFromKitty.0, trumpSuit)
    }

    pub fn SwapCardFromKitty( &mut self, cardFromKitty : &mut Card )
    {
        let gameState = DecideWhatCardToSwapFromKitty_State
        {
            aHand               : self.hand.clone()     ,
            aCardFromTheKitty   : cardFromKitty.clone() ,
        };

        let cardIndexToSwap = self.brain.DecideWhatCardToSwapFromKitty(&gameState);

        SwapCard(&mut self.hand, cardIndexToSwap, cardFromKitty);
    }

    pub fn ChooseTrumpFromKitty( &mut self, cardFromKitty : &Card ) -> bool
    {
        let canPickup = self.CanPickupCardFromKitty(cardFromKitty);

        if !canPickup
        {
            // No decision needed if it can't be legally picked up.
            false
        }
        else
        {
            let gameState = DecideIfIShouldSelectTrumpFromTheKitty_State
            {
                aHand               : self.hand.clone(),
                aCardFromTheKitty   : cardFromKitty.clone(),
            };

            self.brain.DecideIfIShouldSelectTrumpFromTheKitty( &gameState )
        }
    }

    pub fn ChooseTrumpFromHand( &mut self, isForced : bool, topCardOfKitty : &Card ) -> Option<TrumpSuit>
    {
        let gameState = DecideIfShouldSelectTrumpFromFromHand_State
        {
            aHand                    : self.hand.clone(),
            isForced                 : isForced,
            theCardThatWasTurnedDown : topCardOfKitty.clone(),
        };

        self.brain.DecideIfShouldSelectTrumpFromFromHand( &gameState ).map(|x| TrumpSuit(x))
    }

    pub fn PlayCard( &mut self, cardsPlayedSoFar : &[Option<Card>; 4], theStartingPlayerIndex : PlayerIndex, leadSuit : Option<LeadSuit>, trumpSuit : TrumpSuit ) -> Card
    {
        let playableCards = euchre::utils::GetPlayableCards(&self.hand, trumpSuit, leadSuit);

        let playableCardIndices = playableCards.iter()
                                               .enumerate()
                                               .filter(|&(_, canPlay)| *canPlay                   )
                                               .map   (| (i, _      )| (self.hand[i].unwrap(), i) )
                                               .collect::<Vec<_>>();

        assert!( playableCardIndices.is_empty() == false );

        let gameState = DecideWhatCardToPlay_State
        {
            myPlayableCards         : playableCardIndices.clone(),
            myIndex                 : self.index                 ,
            theCardsPlayedSoFar     : cardsPlayedSoFar.clone()   ,
            theStartingPlayerIndex  : theStartingPlayerIndex     ,
            theLeadSuit             : leadSuit                   ,
            theTrumpSuit            : trumpSuit                  ,
        };

        let cardIndex = self.brain.DecideWhatCardToPlay( &gameState );

        assert!(playableCardIndices.iter().position(|x| x.1 == cardIndex).is_some(), "Card index {} isn't valid. Choices were {:?}", cardIndex, playableCardIndices );

        PlayCard( &mut self.hand, cardIndex)
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn Test_CanPickupCardFromKitty()
    {
        unimplemented!();
    }

    #[test]
    fn Test_ChooseTrumpFromKitty()
    {
        unimplemented!();
    }

    #[test]
    fn Test_SwapCardFromKitty()
    {
        use rand;
        use crate::player::brain::random::ThreadRngRandomPlayerBrain;
        use crate::card::*;

        // Setup the game.
        for _ in 0..100
        {
            let deck = NewShuffledCardDeck_ThreadRNG();

            let (hand1, _, _, _, mut kitty) = DealHandsFromDeck(&deck);

            let mut brain = ThreadRngRandomPlayerBrain::New( rand::thread_rng() );

            let mut player = Player::New( &mut brain as &mut dyn crate::player::brain::IPlayerBrain, hand1, 0 );

            let kittyCard : &mut Card = &mut kitty[0];

            // Swap with the kitty.
            let oldKittyCard = kittyCard.clone();
            let oldHand = player.hand.clone();

            player.SwapCardFromKitty(kittyCard);

            let newKittyCard = kittyCard.clone();
            let newHand = player.hand.clone();

            // Make sure it actually did something.
            assert_ne!(oldKittyCard, newKittyCard);
            assert_ne!(oldHand     , newHand     );

            // Make sure the new hand has the top of the kitty in it.
            assert!( newHand.iter().find(|&&x| x == Some(oldKittyCard)).is_some() );

            // Make sure the new card on top of the kitty was from the original hand.
            assert!( oldHand.iter().find(|&&x| x == Some(newKittyCard)).is_some() );
        }
    }

    #[test]
    fn Test_ChooseTrumpFromHand()
    {
        unimplemented!();
    }

    #[test]
    fn Test_PlayCard()
    {
        unimplemented!();
    }
}
