#![allow(non_snake_case)]

use std;
use rand;

use crate::player::brain::*;

pub struct RandomPlayerBrain<R> where R : rand::Rng
{
    pub rng : std::cell::RefCell<R>,
}

pub type ThreadRngRandomPlayerBrain = RandomPlayerBrain<rand::ThreadRng>;

impl<R : rand::Rng> RandomPlayerBrain<R>
{
    pub fn New(rng : R) -> RandomPlayerBrain<R>
    {
        RandomPlayerBrain
        {
            rng : std::cell::RefCell::new(rng)
        }
    }
}

impl<R : rand::Rng> IPlayerBrain for RandomPlayerBrain<R>
{
    fn DecideWhatCardToSwapFromKitty_Impl( &mut self, _ : &DecideWhatCardToSwapFromKitty_State ) -> CardIndex
    {
        let mut rng = self.rng.borrow_mut();

        rng.gen_range(0, 5)
    }

    fn DecideIfIShouldSelectTrumpFromTheKitty_Impl( &mut self, _ : &DecideIfIShouldSelectTrumpFromTheKitty_State ) -> bool
    {
        let mut rng = self.rng.borrow_mut();

        rng.gen::<bool>()
    }

    fn DecideWhatCardToPlay_Impl( &mut self, theGameState : &DecideWhatCardToPlay_State ) -> CardIndex
    {
        let mut rng = self.rng.borrow_mut();

        theGameState.myPlayableCards[ rng.gen_range(0, theGameState.myPlayableCards.len()) ].1
    }

    fn DecideIfShouldSelectTrumpFromFromHand_Impl( &mut self, theGameState : &DecideIfShouldSelectTrumpFromFromHand_State ) -> Option<CardSuit>
    {
        let mut rng = self.rng.borrow_mut();

        if !theGameState.isForced && rng.gen::<bool>()
        {
            return None;
        }

        let randomCardIndex = rng.gen_range(0, 5);

        Some( theGameState.aHand[randomCardIndex].unwrap().0 )
    }
}
