#![allow(non_snake_case)]

use crate::player::brain::*;

pub struct WrapperPlayerBrain<T : IPlayerBrain>
{
    pub brain : T
}

impl<T : IPlayerBrain> WrapperPlayerBrain<T>
{
    pub fn New(brain : T) -> WrapperPlayerBrain<T>
    {
        WrapperPlayerBrain
        {
            brain : brain
        }
    }
}

impl<T : IPlayerBrain> IPlayerBrain for WrapperPlayerBrain<T>
{
    fn DecideWhatCardToSwapFromKitty_Impl( &mut self, theGameState : &DecideWhatCardToSwapFromKitty_State ) -> CardIndex
    {
        self.brain.DecideWhatCardToSwapFromKitty_Impl(theGameState)
    }

    fn DecideIfIShouldSelectTrumpFromTheKitty_Impl( &mut self, theGameState : &DecideIfIShouldSelectTrumpFromTheKitty_State ) -> bool
    {
        self.brain.DecideIfIShouldSelectTrumpFromTheKitty_Impl( theGameState )
    }

    fn DecideWhatCardToPlay_Impl( &mut self, theGameState : &DecideWhatCardToPlay_State ) -> CardIndex
    {
        self.brain.DecideWhatCardToPlay_Impl( theGameState )
    }

    fn DecideIfShouldSelectTrumpFromFromHand_Impl( &mut self, theGameState : &DecideIfShouldSelectTrumpFromFromHand_State ) -> Option<CardSuit>
    {
        self.brain.DecideIfShouldSelectTrumpFromFromHand_Impl( theGameState )
    }
}
