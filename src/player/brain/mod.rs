#![allow(non_snake_case)]
#![allow(non_camel_case_types)]

use crate::card::*;
use crate::euchre::utils::*;

pub mod random;
pub mod human;
pub mod authored;
pub mod wrapper;
pub mod cheater;
pub mod executable_client;
pub mod executable_server;

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct DecideWhatCardToSwapFromKitty_State
{
    pub aHand                       : MutableCardHand           ,
    pub aCardFromTheKitty           : Card                      ,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct DecideIfIShouldSelectTrumpFromTheKitty_State
{
    pub aHand                       : MutableCardHand           ,
    pub aCardFromTheKitty           : Card                      ,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct DecideIfShouldSelectTrumpFromFromHand_State
{
    pub aHand                       : MutableCardHand           ,
    pub isForced                    : bool                      ,
    pub theCardThatWasTurnedDown    : Card                      ,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub struct DecideWhatCardToPlay_State
{
    pub myPlayableCards             : Vec<(Card, CardIndex)>    ,
    pub myIndex                     : PlayerIndex               ,
    pub theCardsPlayedSoFar         : [Option<Card>; 4]         ,
    pub theStartingPlayerIndex      : PlayerIndex               ,
    pub theLeadSuit                 : Option<LeadSuit>          ,
    pub theTrumpSuit                : TrumpSuit                 ,
}

pub trait IPlayerBrain
{
    fn DecideWhatCardToSwapFromKitty( &mut self, theGameState : &DecideWhatCardToSwapFromKitty_State ) -> CardIndex
    {
        let cardIndex = self.DecideWhatCardToSwapFromKitty_Impl(theGameState);

        // Sanity checks.
        {
            let minCardIndex = 0;
            let maxCardIndex = theGameState.aHand.len();

            let cardIndexInRange = (cardIndex >= minCardIndex) && (cardIndex < maxCardIndex);

            assert!( cardIndexInRange, "Invalid card index {}, must be [{}, {}]! input:{:?}", cardIndex, minCardIndex, maxCardIndex, theGameState );

            assert!( theGameState.aHand[cardIndex].is_some(), "Card index {} pointed to invalid card! input:{:?}", cardIndex, theGameState );
        }

        cardIndex
    }
    fn DecideIfIShouldSelectTrumpFromTheKitty( &mut self, theGameState : &DecideIfIShouldSelectTrumpFromTheKitty_State ) -> bool
    {
        // There is no validation because we already checked that they are allowed
        // to select trump if they wanted to, so there is no invalid answer.

        self.DecideIfIShouldSelectTrumpFromTheKitty_Impl(theGameState)
    }

    fn DecideIfShouldSelectTrumpFromFromHand( &mut self, theGameState : &DecideIfShouldSelectTrumpFromFromHand_State ) -> Option<CardSuit>
    {
        let anOptCardSuit = self.DecideIfShouldSelectTrumpFromFromHand_Impl(theGameState);

        // Sanity check.
        if let Some(aCardSuit) = anOptCardSuit
        {
            let hasTrumpSuitInHand = DoesHandHaveSuitInIt(&theGameState.aHand, aCardSuit, None);

            assert!(hasTrumpSuitInHand, "Brain called trump when they didn't have that suit in their hand!")
        }
        else
        {
            assert!( theGameState.isForced == false, "Brain didn't call trump when forced!" );
        }

        anOptCardSuit
    }

    fn DecideWhatCardToPlay( &mut self, theGameState : &DecideWhatCardToPlay_State ) -> CardIndex
    {
        let ret = self.DecideWhatCardToPlay_Impl(theGameState);

        ret
    }

    fn DecideWhatCardToSwapFromKitty_Impl         ( &mut self, theGameState : &DecideWhatCardToSwapFromKitty_State          ) -> CardIndex;
    fn DecideIfIShouldSelectTrumpFromTheKitty_Impl( &mut self, theGameState : &DecideIfIShouldSelectTrumpFromTheKitty_State ) -> bool;
    fn DecideIfShouldSelectTrumpFromFromHand_Impl ( &mut self, theGameState : &DecideIfShouldSelectTrumpFromFromHand_State  ) -> Option<CardSuit>;
    fn DecideWhatCardToPlay_Impl                  ( &mut self, theGameState : &DecideWhatCardToPlay_State                   ) -> CardIndex;
}

#[cfg(test)]
mod tests
{
    use super::random::*;
    use super::authored::*;
    use super::wrapper::*;

    use rand;
    use rand::StdRng;
    use rand::SeedableRng;

    #[test]
    #[allow(unused_variables)]
    fn Test_BrainsCompile()
    {
        // Just making sure that all the different types of brains compile.

        let brain1 = ThreadRngRandomPlayerBrain::New( rand::thread_rng() );

        let seed: &[_] = &[1, 2, 3, 4];
        let rng: StdRng = SeedableRng::from_seed(seed);

        let brain2 = RandomPlayerBrain::New( rng );

        let brain3 = AuthoredPlayerBrain;

        let brain4 = WrapperPlayerBrain::New( ThreadRngRandomPlayerBrain::New(rand::thread_rng()) );

        let brain4 = WrapperPlayerBrain::New( AuthoredPlayerBrain );
    }
}
