#![allow(non_snake_case)]

use crate::player::brain::*;

pub struct AuthoredPlayerBrain;

/*
fn AnalyzeHand(aHand : &[Card; 5], trumpSuit : CardSuit) -> (usize, usize)
{
    use std::collections::HashSet;

    let numTrump = aHand.iter().filter(|aCard| IsTrump(aCard, trumpSuit) ).count();

    let numSuits = aHand.iter().map(|aCard| GetEffectiveSuit(aCard, trumpSuit)).collect::<HashSet<_>>().len();

    (numTrump, numSuits)
}

fn WeightAttributes( someAttributes : (usize, usize) ) -> f32
{
    let weightedNumTrump = ((someAttributes.0 as f32) / 5.0) * 0.75;
    let weightedNumSuits = ( (1.0 - (someAttributes.1 as f32) / 4.0)) * 0.25;

    weightedNumTrump + weightedNumSuits
}
*/

impl IPlayerBrain for AuthoredPlayerBrain
{
    fn DecideWhatCardToSwapFromKitty_Impl( &mut self, _ : &DecideWhatCardToSwapFromKitty_State ) -> CardIndex
    {
        /*
        let baseHand =
        [
            aHand[0].unwrap(),
            aHand[1].unwrap(),
            aHand[2].unwrap(),
            aHand[3].unwrap(),
            aHand[4].unwrap(),
        ];

        //let possibleHands2 = (0..5).map(|i| { let mut base = baseHand.clone(); base[i] = *aCardFromTheKitty; base }).collect::<Vec<_>>();

        //println!("{} {:#?}", aCardFromTheKitty, possibleHands2);

        let mut possibleHands = (0..5).map(|i| { let mut base = baseHand.clone(); base[i] = *aCardFromTheKitty; base } )
                                      .map(|aPossibleHand| AnalyzeHand(&aPossibleHand, aCardFromTheKitty.0) )
                                      .map(|attributes| WeightAttributes(attributes) )
                                      .enumerate()
                                      .collect::<Vec<_>>();

        possibleHands.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap_or(::std::cmp::Ordering::Equal) );

        //println!("{:?}", possibleHands);

        assert!( possibleHands.last().unwrap().1 >= possibleHands.first().unwrap().1 );

        possibleHands.last().unwrap().0
        */

        0
    }

    fn DecideIfIShouldSelectTrumpFromTheKitty_Impl( &mut self, _ : &DecideIfIShouldSelectTrumpFromTheKitty_State ) -> bool
    {
        /*
        use std::io;

        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(n) => {
                println!("{} bytes read", n);
                println!("{}", input);
            }
            Err(error) => println!("error: {}", error),
        }
        */
        false
    }

    fn DecideWhatCardToPlay_Impl( &mut self, theGameState : &DecideWhatCardToPlay_State ) -> CardIndex
    {
        let isPlayingTheFirstCard = theGameState.myIndex == theGameState.theStartingPlayerIndex;

        if isPlayingTheFirstCard
        {
            assert!( theGameState.theCardsPlayedSoFar.iter().all(|anOptCard| anOptCard.is_none()) );

            theGameState.myPlayableCards[ 0 ].1
        }
        else
        {
            let winningPlayer = GetWinningCardIndex( &theGameState.theCardsPlayedSoFar, theGameState.theLeadSuit.unwrap(), theGameState.theTrumpSuit );

            let winningCard = theGameState.theCardsPlayedSoFar[winningPlayer].unwrap();

            let isTeammateWinning = IsSameTeam(winningPlayer, theGameState.myIndex);

            if !isTeammateWinning
            {
                for &(card, index) in theGameState.myPlayableCards.iter()
                {
                    if CompareTwoCards(&card, &winningCard, theGameState.theLeadSuit.unwrap(), theGameState.theTrumpSuit) == ::std::cmp::Ordering::Greater
                    {
                        return index;
                    }
                }
            }

            let mut c = [None; 5];

            for &(card, index) in theGameState.myPlayableCards.iter()
            {
                c[index] = Some(card);
            }

            // Play the "worst" card in the hand

            // NOTE: Can avoid collecting into vector by using iter::max_by when it becomes stable.
            let mut vec : Vec<(CardIndex, Card)> = c.iter()
                                                    .enumerate()
                                                    .filter(|&(_, x)| x.is_some())
                                                    .map(|(i, x)| (i, x.unwrap()) )
                                                    .collect();

            vec.sort_by(|a, b| CompareTwoCards(&a.1, &b.1, theGameState.theLeadSuit.unwrap(), theGameState.theTrumpSuit));

            vec.first().expect("Can't get worst card from empty list!").0
        }
    }

    fn DecideIfShouldSelectTrumpFromFromHand_Impl( &mut self, theGameState : &DecideIfShouldSelectTrumpFromFromHand_State ) -> Option<CardSuit>
    {
        if theGameState.isForced
        {
            Some( theGameState.aHand[0].unwrap().0 )
        }
        else
        {
            None
        }
    }
}
