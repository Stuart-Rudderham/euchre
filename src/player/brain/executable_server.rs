#![allow(non_snake_case)]

use std;
use std::io::BufReader;

use crate::player::brain::*;

pub struct ExecutableServerPlayerBrain
{
    pub childProcess : std::process::Child
}

impl ExecutableServerPlayerBrain
{
    pub fn New(executablePath : &std::ffi::OsStr) -> ExecutableServerPlayerBrain
    {
        ExecutableServerPlayerBrain
        {
            childProcess : std::process::Command::new(&executablePath)
                               .stdin(std::process::Stdio::piped())
                               .stdout(std::process::Stdio::piped())
                               .spawn()
                               .expect( &format!("failed to execute process {:?}", executablePath) )
        }
    }

    pub fn ExecuteCommand(&mut self, command : u8, gameState : &str) -> String
    {
        use std::io::Write;
        use std::io::BufRead;

        // Issue command to client.
        let mut childStdin = self.childProcess.stdin.as_mut().expect("Child process doesn't have an stdin!");

        writeln!(&mut childStdin, "{}|{}", command, gameState).expect("Failed to write to child process!");

        // Read response, blocking the server.
        let childStdout = self.childProcess.stdout.as_mut().expect("Child process doesn't have an stdout!");

        let mut reader = BufReader::new(childStdout);

        let mut line = String::new();
        reader.read_line(&mut line).expect("Failed to read from child process!");

        line
    }
}

impl Drop for ExecutableServerPlayerBrain
{
    fn drop(&mut self)
    {
        // The Rust documentation says that this wait() call is necessary.
        //
        //     Take note that there is no implementation of Drop for child
        //     processes, so if you do not ensure the Child has exited then
        //     it will continue to run, even after the Child handle to the
        //     child process has gone out of scope.
        //
        //     Calling wait (or other functions that wrap around it) will
        //     make the parent process wait until the child has actually
        //     exited before continuing.
        //
        // Testing on Windows, the child process seems to get killed anyways
        // even if wait() isn't called. Leaving it in because it doesn't hurt.
        self.childProcess.wait().expect("command wasn't running");
    }
}

impl IPlayerBrain for ExecutableServerPlayerBrain
{
    fn DecideWhatCardToSwapFromKitty_Impl( &mut self, theGameState : &DecideWhatCardToSwapFromKitty_State ) -> CardIndex
    {
        let encoded = serde_json::to_string(&theGameState).expect("Failed to encode DecideWhatCardToSwapFromKitty_Impl");

        let output = self.ExecuteCommand(0, &encoded);

        //println!("{:?}", output );

        let decoded = serde_json::from_str(&output).expect("Failed to decode DecideWhatCardToSwapFromKitty_Impl");

        decoded
    }

    fn DecideIfIShouldSelectTrumpFromTheKitty_Impl( &mut self, theGameState : &DecideIfIShouldSelectTrumpFromTheKitty_State ) -> bool
    {
        let encoded = serde_json::to_string(&theGameState).expect("Failed to encode DecideIfIShouldSelectTrumpFromTheKitty_Impl");

        let output = self.ExecuteCommand(1, &encoded);

        //println!("{:?}", output );

        let decoded = serde_json::from_str(&output).expect("Failed to decode DecideIfIShouldSelectTrumpFromTheKitty_Impl");

        decoded
    }

    fn DecideWhatCardToPlay_Impl( &mut self, theGameState : &DecideWhatCardToPlay_State ) -> CardIndex
    {
        let encoded = serde_json::to_string(&theGameState).expect("Failed to encode DecideWhatCardToPlay_Impl");

        let output = self.ExecuteCommand(2, &encoded);

        //println!("{:?}", output );

        let decoded = serde_json::from_str(&output).expect("Failed to decode DecideWhatCardToPlay_Impl");

        decoded
    }

    fn DecideIfShouldSelectTrumpFromFromHand_Impl( &mut self, theGameState : &DecideIfShouldSelectTrumpFromFromHand_State ) -> Option<CardSuit>
    {
        let encoded = serde_json::to_string(&theGameState).expect("Failed to encode DecideIfShouldSelectTrumpFromFromHand_Impl");

        let output = self.ExecuteCommand(3, &encoded);

        //println!("{:?}", output );

        let decoded = serde_json::from_str(&output).expect("Failed to decode DecideIfShouldSelectTrumpFromFromHand_Impl");

        decoded
    }
}
