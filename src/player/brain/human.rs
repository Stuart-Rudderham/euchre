#![allow(non_snake_case)]

/*

use std;
use rand;

use card::*;
use euchre::utils::*;

use player::brain::IPlayerBrain;

pub struct HumanPlayerBrain;

impl IPlayerBrain for HumanPlayerBrain
{
    fn DecideWhatCardToSwapFromKitty( &mut self, aHand : &[Option<Card>; 5], aCardFromTheKitty : &Card ) -> CardIndex
    {
        0
    }

    fn DecideIfIShouldSelectTrumpFromTheKitty( &mut self, aCardFromTheKitty : &Card ) -> bool
    {
        /*
        use std::io;

        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(n) => {
                println!("{} bytes read", n);
                println!("{}", input);
            }
            Err(error) => println!("error: {}", error),
        }
        */
        false
    }

    fn DecideWhatCardToPlay( &mut self, aHand : Vec<CardIndex>, leadSuit : Option<CardSuit>, trumpSuit : CardSuit ) -> CardIndex
    {
        aHand[ 0 ]
    }

    fn DecideIfShouldSelectTrumpFromFromHand( &mut self, aHand : &[Option<Card>; 5], isForced : bool, theCardThatWasTurnedDown : &Card ) -> Option<CardSuit>
    {
        if isForced
        {
            Some( aHand[0].unwrap().0 )
        }
        else
        {
            None
        }
    }
}

*/
