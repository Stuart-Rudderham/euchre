#![allow(non_snake_case)]

use crate::player::brain::*;

pub struct CheaterPlayerBrain;

impl IPlayerBrain for CheaterPlayerBrain
{
    fn DecideWhatCardToSwapFromKitty_Impl( &mut self, /*theGameState*/_ : &DecideWhatCardToSwapFromKitty_State ) -> CardIndex
    {
        6
    }

    fn DecideIfIShouldSelectTrumpFromTheKitty_Impl( &mut self, /*theGameState*/_ : &DecideIfIShouldSelectTrumpFromTheKitty_State ) -> bool
    {
        false
    }

    fn DecideWhatCardToPlay_Impl( &mut self, /*theGameState*/_ : &DecideWhatCardToPlay_State ) -> CardIndex
    {
        6
    }

    fn DecideIfShouldSelectTrumpFromFromHand_Impl( &mut self, theGameState : &DecideIfShouldSelectTrumpFromFromHand_State ) -> Option<CardSuit>
    {
        if theGameState.isForced
        {
            None
        }
        else
        {
            None
        }
    }
}
