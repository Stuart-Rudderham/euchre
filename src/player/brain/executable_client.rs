#![allow(non_snake_case)]

use crate::player::brain::*;

pub struct ExecutableClientPlayerBrain<T : IPlayerBrain>
{
    pub brain : T
}

impl<T : IPlayerBrain> ExecutableClientPlayerBrain<T>
{
    pub fn New(brain : T) -> ExecutableClientPlayerBrain<T>
    {
        ExecutableClientPlayerBrain
        {
            brain : brain
        }
    }

    pub fn Run(&mut self)
    {
        use std::io;
        use std::io::prelude::*;

        let stdin = io::stdin();

        for line in stdin.lock().lines()
        {
            let s = line.unwrap();
            let v: Vec<&str> = s.split('|').collect();

            let command : u8 = serde_json::from_str(&v[0]).expect("bot - Failed to decode command");

            let ret = match command
            {
                0 => self.DecideWhatCardToSwapFromKitty_Impl         (&v[1]),
                1 => self.DecideIfIShouldSelectTrumpFromTheKitty_Impl(&v[1]),
                2 => self.DecideWhatCardToPlay_Impl                  (&v[1]),
                3 => self.DecideIfShouldSelectTrumpFromFromHand_Impl (&v[1]),
                _ => panic!("unknown command {:?}", command ),
            };

            println!("{}", ret);
        }
    }

    fn DecideWhatCardToSwapFromKitty_Impl( &mut self, theGameStateStr : &str ) -> String
    {
        let gameState : DecideWhatCardToSwapFromKitty_State = serde_json::from_str(theGameStateStr).expect("bot - Failed to decode DecideWhatCardToSwapFromKitty_Impl");

        let ret = self.brain.DecideWhatCardToSwapFromKitty_Impl(&gameState);

        serde_json::to_string(&ret).expect("Failed to encode DecideWhatCardToSwapFromKitty_Impl")
    }

    fn DecideIfIShouldSelectTrumpFromTheKitty_Impl( &mut self, theGameStateStr : &str ) -> String
    {
        let gameState : DecideIfIShouldSelectTrumpFromTheKitty_State = serde_json::from_str(theGameStateStr).expect("bot - Failed to decode DecideWhatCardToSwapFromKitty_Impl");

        let ret = self.brain.DecideIfIShouldSelectTrumpFromTheKitty_Impl(&gameState);

        serde_json::to_string(&ret).expect("Failed to encode DecideWhatCardToSwapFromKitty_Impl")
    }

    fn DecideWhatCardToPlay_Impl( &mut self, theGameStateStr : &str ) -> String
    {
        let gameState : DecideWhatCardToPlay_State = serde_json::from_str(theGameStateStr).expect("bot - Failed to decode DecideWhatCardToSwapFromKitty_Impl");

        let ret = self.brain.DecideWhatCardToPlay_Impl(&gameState);

        serde_json::to_string(&ret).expect("Failed to encode DecideWhatCardToSwapFromKitty_Impl")
    }

    fn DecideIfShouldSelectTrumpFromFromHand_Impl( &mut self, theGameStateStr : &str ) -> String
    {
        let gameState : DecideIfShouldSelectTrumpFromFromHand_State = serde_json::from_str(theGameStateStr).expect("bot - Failed to decode DecideWhatCardToSwapFromKitty_Impl");

        let ret = self.brain.DecideIfShouldSelectTrumpFromFromHand_Impl(&gameState);

        serde_json::to_string(&ret).expect("Failed to encode DecideWhatCardToSwapFromKitty_Impl")
    }
}
