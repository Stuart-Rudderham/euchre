#![allow(non_snake_case)]

#[derive(Debug, PartialEq, Eq, Copy, Clone, Hash, serde::Deserialize, serde::Serialize)]
pub enum CardSuit
{
    Clubs       ,
    Diamonds    ,
    Hearts      ,
    Spades      ,
}

#[derive(Debug, PartialEq, Eq, Copy, Clone, Hash, serde::Deserialize, serde::Serialize)]
pub struct TrumpSuit(pub CardSuit);

#[derive(Debug, PartialEq, Eq, Copy, Clone, Hash, serde::Deserialize, serde::Serialize)]
pub struct LeadSuit (pub CardSuit);

#[allow(dead_code)]
pub static CARD_SUIT_LIST : [CardSuit;  4] =
    [
        CardSuit::Clubs     ,
        CardSuit::Diamonds  ,
        CardSuit::Hearts    ,
        CardSuit::Spades    ,
    ];

impl ::std::fmt::Display for CardSuit
{
    fn fmt( &self, f : &mut ::std::fmt::Formatter ) -> ::std::fmt::Result
    {
        let s = match *self
        {
            CardSuit::Clubs    => "♣",
            CardSuit::Diamonds => "♦",
            CardSuit::Hearts   => "♥",
            CardSuit::Spades   => "♠",
        };

        write!( f, "{}", s )
    }
}

#[derive(Debug, PartialEq, Eq, Copy, Clone, Hash, serde::Deserialize, serde::Serialize)]
pub enum CardValue
{
    Nine        ,
    Ten         ,
    Jack        ,
    Queen       ,
    King        ,
    Ace         ,
}

#[allow(dead_code)]
pub static CARD_VALUE_LIST : [CardValue;  6] =
    [
        CardValue::Nine ,
        CardValue::Ten  ,
        CardValue::Jack ,
        CardValue::Queen,
        CardValue::King ,
        CardValue::Ace  ,
    ];

impl ::std::fmt::Display for CardValue
{
    fn fmt( &self, f : &mut ::std::fmt::Formatter ) -> ::std::fmt::Result
    {
        let s = match *self
        {
            CardValue::Nine  => "9" ,
            CardValue::Ten   => "10",
            CardValue::Jack  => "J" ,
            CardValue::Queen => "Q" ,
            CardValue::King  => "K" ,
            CardValue::Ace   => "A" ,
        };

        write!( f, "{}", s )
    }
}

#[derive(Debug, PartialEq, Eq, Copy, Clone, Hash, serde::Deserialize, serde::Serialize)]
pub struct Card( pub CardSuit, pub CardValue );

#[allow(dead_code)] pub const CLUBS_9     : Card = Card( CardSuit::Clubs   , CardValue::Nine  );
#[allow(dead_code)] pub const CLUBS_10    : Card = Card( CardSuit::Clubs   , CardValue::Ten   );
#[allow(dead_code)] pub const CLUBS_J     : Card = Card( CardSuit::Clubs   , CardValue::Jack  );
#[allow(dead_code)] pub const CLUBS_Q     : Card = Card( CardSuit::Clubs   , CardValue::Queen );
#[allow(dead_code)] pub const CLUBS_K     : Card = Card( CardSuit::Clubs   , CardValue::King  );
#[allow(dead_code)] pub const CLUBS_A     : Card = Card( CardSuit::Clubs   , CardValue::Ace   );
#[allow(dead_code)] pub const DIAMONDS_9  : Card = Card( CardSuit::Diamonds, CardValue::Nine  );
#[allow(dead_code)] pub const DIAMONDS_10 : Card = Card( CardSuit::Diamonds, CardValue::Ten   );
#[allow(dead_code)] pub const DIAMONDS_J  : Card = Card( CardSuit::Diamonds, CardValue::Jack  );
#[allow(dead_code)] pub const DIAMONDS_Q  : Card = Card( CardSuit::Diamonds, CardValue::Queen );
#[allow(dead_code)] pub const DIAMONDS_K  : Card = Card( CardSuit::Diamonds, CardValue::King  );
#[allow(dead_code)] pub const DIAMONDS_A  : Card = Card( CardSuit::Diamonds, CardValue::Ace   );
#[allow(dead_code)] pub const HEARTS_9    : Card = Card( CardSuit::Hearts  , CardValue::Nine  );
#[allow(dead_code)] pub const HEARTS_10   : Card = Card( CardSuit::Hearts  , CardValue::Ten   );
#[allow(dead_code)] pub const HEARTS_J    : Card = Card( CardSuit::Hearts  , CardValue::Jack  );
#[allow(dead_code)] pub const HEARTS_Q    : Card = Card( CardSuit::Hearts  , CardValue::Queen );
#[allow(dead_code)] pub const HEARTS_K    : Card = Card( CardSuit::Hearts  , CardValue::King  );
#[allow(dead_code)] pub const HEARTS_A    : Card = Card( CardSuit::Hearts  , CardValue::Ace   );
#[allow(dead_code)] pub const SPADES_9    : Card = Card( CardSuit::Spades  , CardValue::Nine  );
#[allow(dead_code)] pub const SPADES_10   : Card = Card( CardSuit::Spades  , CardValue::Ten   );
#[allow(dead_code)] pub const SPADES_J    : Card = Card( CardSuit::Spades  , CardValue::Jack  );
#[allow(dead_code)] pub const SPADES_Q    : Card = Card( CardSuit::Spades  , CardValue::Queen );
#[allow(dead_code)] pub const SPADES_K    : Card = Card( CardSuit::Spades  , CardValue::King  );
#[allow(dead_code)] pub const SPADES_A    : Card = Card( CardSuit::Spades  , CardValue::Ace   );

impl ::std::fmt::Display for Card
{
    fn fmt( &self, f : &mut ::std::fmt::Formatter ) -> ::std::fmt::Result
    {
        write!( f, "{}{}", self.0, self.1 )
    }
}

pub type CardDeck = [Card; 24];

pub fn ShuffleCardDeck<R : ::rand::Rng>(aDeck : &mut CardDeck, rng : &mut R)
{
    rng.shuffle(aDeck);
}

pub fn NewUnshuffledCardDeck() -> CardDeck
{
    [
        Card( CardSuit::Clubs   , CardValue::Nine  ),
        Card( CardSuit::Clubs   , CardValue::Ten   ),
        Card( CardSuit::Clubs   , CardValue::Jack  ),
        Card( CardSuit::Clubs   , CardValue::Queen ),
        Card( CardSuit::Clubs   , CardValue::King  ),
        Card( CardSuit::Clubs   , CardValue::Ace   ),
        Card( CardSuit::Diamonds, CardValue::Nine  ),
        Card( CardSuit::Diamonds, CardValue::Ten   ),
        Card( CardSuit::Diamonds, CardValue::Jack  ),
        Card( CardSuit::Diamonds, CardValue::Queen ),
        Card( CardSuit::Diamonds, CardValue::King  ),
        Card( CardSuit::Diamonds, CardValue::Ace   ),
        Card( CardSuit::Hearts  , CardValue::Nine  ),
        Card( CardSuit::Hearts  , CardValue::Ten   ),
        Card( CardSuit::Hearts  , CardValue::Jack  ),
        Card( CardSuit::Hearts  , CardValue::Queen ),
        Card( CardSuit::Hearts  , CardValue::King  ),
        Card( CardSuit::Hearts  , CardValue::Ace   ),
        Card( CardSuit::Spades  , CardValue::Nine  ),
        Card( CardSuit::Spades  , CardValue::Ten   ),
        Card( CardSuit::Spades  , CardValue::Jack  ),
        Card( CardSuit::Spades  , CardValue::Queen ),
        Card( CardSuit::Spades  , CardValue::King  ),
        Card( CardSuit::Spades  , CardValue::Ace   ),
    ]
}

pub fn NewShuffledCardDeck<R : ::rand::Rng>(rng : &mut R) -> CardDeck
{
    let mut deck = NewUnshuffledCardDeck();

    ShuffleCardDeck(&mut deck, rng);

    deck
}

#[cfg(test)]
pub fn NewShuffledCardDeck_ThreadRNG() -> CardDeck
{
    let rng = &mut ::rand::thread_rng();

    NewShuffledCardDeck(rng)
}

pub type CardIndex = usize;

pub type CardHand = [Card; 5];
pub type CardKitty = [Card; 4];

pub type DealtCards = (CardHand, CardHand, CardHand, CardHand, CardKitty);

pub fn DealHandsFromDeck(aDeck : &CardDeck) -> DealtCards
{
    (
        [ aDeck[ 0], aDeck[ 1], aDeck[ 2], aDeck[ 3], aDeck[ 4] ],
        [ aDeck[ 5], aDeck[ 6], aDeck[ 7], aDeck[ 8], aDeck[ 9] ],
        [ aDeck[10], aDeck[11], aDeck[12], aDeck[13], aDeck[14] ],
        [ aDeck[15], aDeck[16], aDeck[17], aDeck[18], aDeck[19] ],
        [ aDeck[20], aDeck[21], aDeck[22], aDeck[23]            ],
    )
}

pub type MutableCardHand = [Option<Card>; 5]; // Vec<Card>

pub fn CreateMutableCardHand(aHand : CardHand) -> MutableCardHand
{
    [
        Some( aHand[0] ),
        Some( aHand[1] ),
        Some( aHand[2] ),
        Some( aHand[3] ),
        Some( aHand[4] ),
    ]
}

pub fn SwapCard( aHand : &mut MutableCardHand, aCardIndexToSwapOut : CardIndex, aCardToSwapIn : &mut Card )
{
    let aCardToSwapOut = aHand[aCardIndexToSwapOut].as_mut().expect("Invalid hand index!");

    ::std::mem::swap( aCardToSwapOut, aCardToSwapIn );

    // Uncomment if hand is switched to Vec<Card>.
    /*
    let aCardToSwapOut = aHand.get_mut(aCardIndexToSwapOut).expect("Invalid hand index!");

    ::std::mem::swap( aCardToSwapOut, aCardToSwapIn );
    */
}

pub fn PlayCard( aHand : &mut MutableCardHand, aCardIndexToPlay : CardIndex ) -> Card
{
    let ret = aHand[aCardIndexToPlay].unwrap().clone();

    aHand[aCardIndexToPlay] = None;

    return ret;

    // Uncomment if hand is switched to Vec<Card>.
    /*
    aHand.remove(aCardIndexToPlay)
    */
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn Test_SwapCard()
    {
        // Setup.
        let mut hand = CreateMutableCardHand([CLUBS_9, HEARTS_10, SPADES_J, DIAMONDS_Q, CLUBS_K]);
        let mut card = CLUBS_A;

        // Index 0.
        SwapCard(&mut hand, 0, &mut card);

        assert_eq!( hand, CreateMutableCardHand([CLUBS_A, HEARTS_10, SPADES_J, DIAMONDS_Q, CLUBS_K]) );
        assert_eq!( card, CLUBS_9 );

        // Index 1.
        SwapCard(&mut hand, 1, &mut card);

        assert_eq!( hand, CreateMutableCardHand([CLUBS_A, CLUBS_9, SPADES_J, DIAMONDS_Q, CLUBS_K]) );
        assert_eq!( card, HEARTS_10 );

        // Index 2.
        SwapCard(&mut hand, 2, &mut card);

        assert_eq!( hand, CreateMutableCardHand([CLUBS_A, CLUBS_9, HEARTS_10, DIAMONDS_Q, CLUBS_K]) );
        assert_eq!( card, SPADES_J );

        // Index 3.
        SwapCard(&mut hand, 3, &mut card);

        assert_eq!( hand, CreateMutableCardHand([CLUBS_A, CLUBS_9, HEARTS_10, SPADES_J, CLUBS_K]) );
        assert_eq!( card, DIAMONDS_Q );

        // Index 4.
        SwapCard(&mut hand, 4, &mut card);

        assert_eq!( hand, CreateMutableCardHand([CLUBS_A, CLUBS_9, HEARTS_10, SPADES_J, DIAMONDS_Q]) );
        assert_eq!( card, CLUBS_K );
    }

    #[test]
    fn Test_PlayCard()
    {
        // Setup.
        let mut hand = CreateMutableCardHand([CLUBS_9, HEARTS_10, SPADES_J, DIAMONDS_Q, CLUBS_K]);

        assert_eq!( PlayCard(&mut hand, 0), CLUBS_9    ); assert_eq!( hand[0], None );
        assert_eq!( PlayCard(&mut hand, 1), HEARTS_10  ); assert_eq!( hand[1], None );
        assert_eq!( PlayCard(&mut hand, 2), SPADES_J   ); assert_eq!( hand[2], None );
        assert_eq!( PlayCard(&mut hand, 3), DIAMONDS_Q ); assert_eq!( hand[3], None );
        assert_eq!( PlayCard(&mut hand, 4), CLUBS_K    ); assert_eq!( hand[4], None );

        // Uncomment if hand is switched to Vec<Card>.
        /*
        assert_eq!( PlayCard(hand, 0), CLUBS_9    );
        assert_eq!( PlayCard(hand, 0), HEARTS_10  );
        assert_eq!( PlayCard(hand, 0), SPADES_J   );
        assert_eq!( PlayCard(hand, 0), DIAMONDS_Q );
        assert_eq!( PlayCard(hand, 0), CLUBS_K    );
        */
    }

    #[test]
    fn Test_UnshuffledDeck()
    {
        let unshuffledDeck = NewUnshuffledCardDeck();

        let mut cards = ::std::collections::HashSet::new();

        // Make sure all cards are unique.
        for aCard in unshuffledDeck.iter()
        {
            assert_eq!(cards.insert(aCard), true);
        }
    }

    #[test]
    fn Test_ShuffledDeck()
    {
        let rng = &mut ::rand::thread_rng();

        let unshuffledDeck = NewUnshuffledCardDeck();
        let   shuffledDeck = NewShuffledCardDeck(rng);

        // Assuming the shuffle function works correctly the odds of
        // this assert firing are 1 in 24! (620,448,401,733,239,439,360,000).
        assert_ne!( unshuffledDeck, shuffledDeck );

        // Make sure both decks have the same cards.
        for aCard in unshuffledDeck.iter()
        {
            assert!( shuffledDeck.iter().find(|&x| x == aCard).is_some() );
        }

        for aCard in shuffledDeck.iter()
        {
            assert!( unshuffledDeck.iter().find(|&x| x == aCard).is_some() );
        }
    }

    #[test]
    fn Test_DealingFromDeck()
    {
        // Setup the game.
        let deck = NewShuffledCardDeck_ThreadRNG();

        let (hand1, hand2, hand3, hand4, kitty) = DealHandsFromDeck(&deck);

        let mut cards = ::std::collections::HashSet::new();

        // Make sure there are no duplicate cards in the dealt cards.
        for aCard in hand1.iter().chain(&hand2)
                                 .chain(&hand3)
                                 .chain(&hand4)
                                 .chain(&kitty)
        {
            assert_eq!(cards.insert(aCard), true);
        }

        // Make sure all the cards in the deck have been dealt out.
        for aCard in deck.iter()
        {
            assert_eq!(cards.insert(aCard), false);
        }
    }
}
