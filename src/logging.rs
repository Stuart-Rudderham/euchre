#![allow(non_snake_case)]

use itertools::izip;
use crate::card::*;
use crate::euchre::utils::PlayerIndex;

#[derive(Debug)]
pub struct GameState
{
    pub hand1 : MutableCardHand,
    pub hand2 : MutableCardHand,
    pub hand3 : MutableCardHand,
    pub hand4 : MutableCardHand,
    pub kitty : CardKitty,

    pub cardsInPlay : [Option<Card>; 4],

    pub score : [u8; 2],

    pub trump : Option<TrumpSuit>
}

impl GameState
{
    pub fn Write(&self) -> Vec<String>
    {
        fn CardToString( aCard : Card ) -> String
        {
            format!("{:3}", format!("{}", aCard))
        }

        fn OptCardSliceToString(aHand : &[Option<Card>], s : String) -> Vec<String>
        {
            aHand.iter().map(|anOptCard| anOptCard.map_or(s.clone(), CardToString)).collect::<Vec<_>>()
        }

        fn CardSliceToString(aHand : &[Card]) -> Vec<String>
        {
            aHand.iter().map(|&aCard| CardToString(aCard)).collect::<Vec<_>>()
        }

        let hand1 = OptCardSliceToString(&self.hand1, "---".to_string());
        let hand2 = OptCardSliceToString(&self.hand2, "---".to_string());
        let hand3 = OptCardSliceToString(&self.hand3, "---".to_string());
        let hand4 = OptCardSliceToString(&self.hand4, "---".to_string());

        let kitty = CardSliceToString(&self.kitty);

        let cardsInPlay = OptCardSliceToString(&self.cardsInPlay, "   ".to_string());

        let trump = self.trump.map_or("-".to_string(), |x| format!("{}", x.0));

        vec!
        [
            format!("+-----------------------------------+"                                                                            ),
            format!("|                                   |"                                                                            ),
            format!("| Kitty {} {} {} {}     NS - {:2} |"        , kitty[0],       kitty[1],       kitty[2], kitty[3], self.score[0]   ),
            format!("| Trump {}                   EW - {:2} |"   , trump   ,  self.score[1],                                           ),
            format!("|                                   |"                                                                            ),
            format!("|        {} {} {} {} {}        |"           , hand1[0],       hand1[1],       hand1[2], hand1[3],      hand1[4]   ),
            format!("| {}                           {} |"        , hand4[0],       hand2[0]                                            ),
            format!("| {}            {}            {} |"         , hand4[1], cardsInPlay[0],       hand2[1]                            ),
            format!("| {}        {}     {}        {} |"          , hand4[2], cardsInPlay[3], cardsInPlay[1], hand2[2]                  ),
            format!("| {}            {}            {} |"         , hand4[3], cardsInPlay[2],       hand2[3]                            ),
            format!("| {}                           {} |"        , hand4[4],       hand2[4]                                            ),
            format!("|        {} {} {} {} {}        |"           , hand3[0],       hand3[1],       hand3[2], hand3[3],      hand3[4]   ),
            format!("|                                   |"                                                                            ),
            format!("+-----------------------------------+"                                                                            ),
        ]
    }
}

pub struct GameStateRecorder
{
    pub initial : Option<GameState>,
    pub hands : Vec<Vec<GameState>>
}

impl GameStateRecorder
{
    pub fn Write(&self) -> Vec<String>
    {
        let mut ret = vec![];

        let init = self.initial.as_ref().unwrap().Write();

        ret.push("Initial".to_string());

        for x in init
        {
            ret.push( x.clone() );
        }

        ret.push("\nHands".to_string());

        for array in self.hands.iter()
        {
            assert_eq!(array.len(), 4);

            for (a, b,c ,d) in izip!(array[0].Write(),
                                     array[1].Write(),
                                     array[2].Write(),
                                     array[3].Write())
            {
                ret.push( format!("{} {} {} {}", a, b, c, d) );
            }

            ret.push( "".to_string() )
        }

        ret
    }
}


pub struct PickingTrumpFromKittyState
{
    pub topOfKitty : Card,
    pub cardSwitchedWithKitty : Option<Card>,
}

pub struct PickingTrumpFromHandState
{

}

pub struct PickingTrumpState
{
    pub startingPlayer : PlayerIndex,
    pub fromKitty : PickingTrumpFromKittyState,
    pub fromHand : Option<PickingTrumpFromHandState>,
    pub callingPlayer : PlayerIndex,
    pub wentAlone : bool,
    pub wasStuck : bool,
    pub trump : TrumpSuit,
}

pub struct SingleHandState
{
    pub startingPlayer : PlayerIndex,
    pub cardsPlayed : [Option<Card>; 4],
    pub winningPlayer : PlayerIndex, // Could probably derive this? But that takes logic, and logic can have bugs.
}

pub struct SingleGameState
{
    pub initialScore : [u8; 2],
    pub dealer : PlayerIndex,
    pub dealtDeck : DealtCards,
    pub pickingTrump : PickingTrumpState,
    pub hands : [SingleHandState; 5],
    pub afterScore : [u8; 2],
}

pub struct EntireGameState
{
    pub initialDealer : PlayerIndex,
    pub games : Vec<SingleGameState>,
    pub finalScore : [u8; 2],
}
