#![allow(non_snake_case)]

extern crate euchre;

use euchre::player::brain::executable_client::ExecutableClientPlayerBrain;
use euchre::player::brain::authored::AuthoredPlayerBrain;

fn main()
{
    let brain = AuthoredPlayerBrain;

    let mut wrappedBrain = ExecutableClientPlayerBrain::New(brain);

    wrappedBrain.Run();
}
