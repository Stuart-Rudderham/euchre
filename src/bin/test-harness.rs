#![allow(non_snake_case)]

extern crate euchre;

extern crate itertools;
extern crate rand;
extern crate skill_rating;
extern crate bbt;

//use euchre::player::brain::random::ThreadRngRandomPlayerBrain;
//use euchre::player::brain::authored::AuthoredPlayerBrain;
//use euchre::player::brain::wrapper::WrapperPlayerBrain;
use euchre::player::brain::executable_server::ExecutableServerPlayerBrain;

use std::ffi::OsStr;

fn main()
{
    let mut brain1 = ExecutableServerPlayerBrain::New(OsStr::new("./authored-bot.exe"));//ThreadRngRandomPlayerBrain::New( rand::thread_rng() );
    let mut brain2 = ExecutableServerPlayerBrain::New(OsStr::new("./random-bot.exe"));//AuthoredPlayerBrain;
    let mut brain3 = ExecutableServerPlayerBrain::New(OsStr::new("./authored-bot.exe"));//ThreadRngRandomPlayerBrain::New( rand::thread_rng() );
    let mut brain4 = ExecutableServerPlayerBrain::New(OsStr::new("./random-bot.exe")); //AuthoredPlayerBrain;

    let mut playerBrains =
    [
        &mut brain1 as &mut dyn euchre::player::brain::IPlayerBrain,
        &mut brain2 as &mut dyn euchre::player::brain::IPlayerBrain,
        &mut brain3 as &mut dyn euchre::player::brain::IPlayerBrain,
        &mut brain4 as &mut dyn euchre::player::brain::IPlayerBrain,
    ];

    // Single game print out.
    if false
    {
        let mut recorder = euchre::logging::GameStateRecorder { initial : None, hands : vec![] };

        euchre::euchre::game::PlaySingleGame(&mut playerBrains, 0, &mut rand::thread_rng(), &mut recorder);

        println!("");
        for s in recorder.Write()
        {
            println!("{}", s);
        }
        println!("");
    }

    // Play lots and lots of games.
    if true
    {
        let mut gamesWon = (0, 0);

        let mut elo = (1000, 1000);

        let rater = bbt::Rater::default();
        let mut bbtRating = (bbt::Rating::default(), bbt::Rating::default());

        for _ in 0..100
        {
            let mut recorder = euchre::logging::GameStateRecorder { initial : None, hands : vec![] };

            let score = euchre::euchre::game::PlayEntireGame(&mut playerBrains, &mut rand::thread_rng(), &mut recorder);

            let winner = if score[0] > score[1]
            {
                gamesWon.0 += 1;

                (skill_rating::elo::WIN, bbt::Outcome::Win)
            }
            else
            {
                gamesWon.1 += 1;

                (skill_rating::elo::LOSS, bbt::Outcome::Loss)
            };

            elo = skill_rating::elo::systems::game_icc(elo.0, elo.1, winner.0 );
            bbtRating = rater.duel(bbtRating.0, bbtRating.1, winner.1);

            //println!("{:?} | {:?}", score, gamesWon);
        }

        println!("Games          : {:?}", gamesWon);
        println!("Elo            : {:?}", elo);
        println!("Elo (expected) : {:?}", skill_rating::elo::expected_score(elo.0, elo.1) * 100_f32);
        println!("BBT            : {:?}", bbtRating);
    }
}
