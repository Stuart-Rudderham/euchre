#![allow(non_snake_case)]

extern crate euchre;
extern crate rand;

use euchre::player::brain::executable_client::ExecutableClientPlayerBrain;
use euchre::player::brain::random::RandomPlayerBrain;

fn main()
{
    let brain = RandomPlayerBrain::New( rand::thread_rng() );

    let mut wrappedBrain = ExecutableClientPlayerBrain::New(brain);

    wrappedBrain.Run();
}
