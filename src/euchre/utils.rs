#![allow(non_snake_case)]

use std;

use crate::card::*;

pub type PlayerIndex = usize;
pub type TeamIndex = usize;

pub fn GetPlayableCards( aHand : &[Option<Card>; 5], theTrumpSuit : TrumpSuit, theOptLeadSuit : Option<LeadSuit> ) -> [bool; 5]
{
    // Can't play until proven otherwise...
    let mut ret = [false; 5];

    // First pass over hand.
    let hasAnyCardsOfLeadSuit = if let Some(theLeadSuit) = theOptLeadSuit
    {
        aHand.iter().any(|anOptCard| anOptCard.map_or(false, |aCard| GetEffectiveSuit(&aCard, theTrumpSuit) == theLeadSuit.0))
    }
    else
    {
        false
    };

    // Second pass over hand.
    for (canPlay, anOptCard) in ret.iter_mut().zip(aHand)
    {
        if let &Some(aCard) = anOptCard
        {
            *canPlay = if hasAnyCardsOfLeadSuit
            {
                // Have to play lead suit if we have any.
                GetEffectiveSuit(&aCard, theTrumpSuit) == theOptLeadSuit.unwrap().0
            }
            else
            {
                // Otherwise can play anything.
                true
            };
        }
    }

    // Sanity check.
    assert!( !ret.iter().all(|&x| x == false), "Logic error! You should always have at least 1 card to play! hand:{:?} trump:{:?} lead:{:?}", aHand, theTrumpSuit, theOptLeadSuit );

    ret
}

pub fn IsValidPlayerIndex( aPlayerIndex : PlayerIndex ) -> bool
{
    aPlayerIndex < 4
}

fn IsValidTeamIndex( aTeamIndex : TeamIndex ) -> bool
{
    aTeamIndex < 2
}

pub fn GetTeam( aPlayerIndex : PlayerIndex ) -> TeamIndex
{
    assert!( IsValidPlayerIndex(aPlayerIndex), "Player {} isn't valid!", aPlayerIndex );

    aPlayerIndex % 2
}

pub fn GetOtherTeam( aTeamIndex : TeamIndex ) -> TeamIndex
{
    assert!( IsValidTeamIndex(aTeamIndex), "Team {} isn't valid!", aTeamIndex );

    return (aTeamIndex + 1) % 2
}

pub fn IsSameTeam( aPlayerIndex1 : PlayerIndex, aPlayerIndex2 : PlayerIndex ) -> bool
{
    GetTeam(aPlayerIndex1) == GetTeam(aPlayerIndex2)
}

// Only used by tests currently.
#[cfg(test)]
pub fn GetPartner( aPlayerIndex : PlayerIndex ) -> PlayerIndex
{
    assert!( IsValidPlayerIndex(aPlayerIndex), "Player {} isn't valid!", aPlayerIndex );

    return (aPlayerIndex + 2) % 4
}

pub fn CalculateScore( handsWon : &[u8; 2], thePlayerThatCalledTrump : PlayerIndex, playerWentAlone : bool ) -> [u8; 2]
{
    assert_eq!( handsWon.iter().sum::<u8>(), 5, "Logic error! Did not play a full game! handsWon:{:?}", handsWon );

    let theTeamThatCalledTrump = GetTeam(thePlayerThatCalledTrump);

    let theTeamThatDidNotCallTrump = GetOtherTeam(theTeamThatCalledTrump);

    let mut score = [0, 0];

    if handsWon[theTeamThatDidNotCallTrump] > handsWon[theTeamThatCalledTrump]
    {
        // Euchre!
        score[theTeamThatDidNotCallTrump] = 2;
    }
    else
    {
        let gotAllTricks = handsWon[theTeamThatCalledTrump] == 5;

        score[theTeamThatCalledTrump] = match (playerWentAlone, gotAllTricks)
        {
            (true , true ) => 4,
            (true , false) => 1,
            (false, true ) => 2,
            (false, false) => 1,
        }
    }

    score
}

pub fn GetOtherColorSuit( aCardSuit : CardSuit ) -> CardSuit
{
    match aCardSuit
    {
        CardSuit::Clubs    => CardSuit::Spades  ,
        CardSuit::Diamonds => CardSuit::Hearts  ,
        CardSuit::Hearts   => CardSuit::Diamonds,
        CardSuit::Spades   => CardSuit::Clubs   ,
    }
}

pub fn IsLeftBower( aCard : &Card, theTrumpSuit : TrumpSuit ) -> bool
{
    aCard.1 == CardValue::Jack && aCard.0 == GetOtherColorSuit(theTrumpSuit.0)
}

pub fn GetEffectiveSuit( aCard : &Card, theTrumpSuit : TrumpSuit ) -> CardSuit
{
    if IsLeftBower(aCard, theTrumpSuit)
    {
        GetOtherColorSuit(aCard.0)
    }
    else
    {
        aCard.0
    }
}

pub fn IsTrump(aCard : &Card, theTrumpSuit : TrumpSuit) -> bool
{
    GetEffectiveSuit(aCard, theTrumpSuit) == theTrumpSuit.0
}

pub fn IsLeadSuit(aCard : &Card, theTrumpSuit : TrumpSuit, theLeadSuit : LeadSuit) -> bool
{
    GetEffectiveSuit(aCard, theTrumpSuit) == theLeadSuit.0
}

pub fn CompareTwoCards(lhs : &Card, rhs : &Card, theLeadSuit : LeadSuit, theTrumpSuit : TrumpSuit) -> std::cmp::Ordering
{
    use crate::card::CardValue::*;
    use std::cmp::Ordering::*;

    let Compare_BothTrump = ||
    {
        // Check preconditions.
        assert_ne!(lhs, rhs);

        assert!( IsTrump(lhs, theTrumpSuit) );
        assert!( IsTrump(rhs, theTrumpSuit) );

        // Exhaustive comparison.
             if lhs.0 == theTrumpSuit.0 && lhs.1 == Jack  { return Greater; } // Right bower.
        else if rhs.0 == theTrumpSuit.0 && rhs.1 == Jack  { return Less   ; }
        else if lhs.0 != theTrumpSuit.0 && lhs.1 == Jack  { return Greater; } // Left bower.
        else if rhs.0 != theTrumpSuit.0 && rhs.1 == Jack  { return Less   ; }
        else if                            lhs.1 == Ace   { return Greater; } // Ace.
        else if                            rhs.1 == Ace   { return Less   ; }
        else if                            lhs.1 == King  { return Greater; } // King.
        else if                            rhs.1 == King  { return Less   ; }
        else if                            lhs.1 == Queen { return Greater; } // Queen.
        else if                            rhs.1 == Queen { return Less   ; }
        else if                            lhs.1 == Ten   { return Greater; } // Ten.
        else if                            rhs.1 == Ten   { return Less   ; }
        else if                            lhs.1 == Nine  { return Greater; } // Nine.
        else if                            rhs.1 == Nine  { return Less   ; }

        panic!("Logic error! None exhaustive match! {:?} {:?} {:?}", lhs, rhs, theTrumpSuit);
    };

    let Compare_SameSuitNeitherTrump = ||
    {
        // Check preconditions.
        assert_ne!(lhs, rhs);

        assert!( !IsTrump(lhs, theTrumpSuit) );
        assert!( !IsTrump(rhs, theTrumpSuit) );

        assert!( GetEffectiveSuit(lhs, theTrumpSuit) == GetEffectiveSuit(rhs, theTrumpSuit) );

        // Exhaustive comparison.
             if lhs.1 == Ace   { return Greater; } // Ace.
        else if rhs.1 == Ace   { return Less   ; }
        else if lhs.1 == King  { return Greater; } // King.
        else if rhs.1 == King  { return Less   ; }
        else if lhs.1 == Queen { return Greater; } // Queen.
        else if rhs.1 == Queen { return Less   ; }
        else if lhs.1 == Jack  { return Greater; } // Jack.
        else if rhs.1 == Jack  { return Less   ; }
        else if lhs.1 == Ten   { return Greater; } // Ten.
        else if rhs.1 == Ten   { return Less   ; }
        else if lhs.1 == Nine  { return Greater; } // Nine.
        else if rhs.1 == Nine  { return Less   ; }

        panic!("Logic error! None exhaustive match! {:?} {:?} {:?}", lhs, rhs, theTrumpSuit);
    };

    let Compare_NeitherTrump = ||
    {
        // Check preconditions.
        assert_ne!(lhs, rhs);

        assert!( !IsTrump(lhs, theTrumpSuit) );
        assert!( !IsTrump(rhs, theTrumpSuit) );

        let lhsLeadSuit = IsLeadSuit(lhs, theTrumpSuit, theLeadSuit);
        let rhsLeadSuit = IsLeadSuit(rhs, theTrumpSuit, theLeadSuit);

        match (lhsLeadSuit, rhsLeadSuit)
        {
            (true , true ) => Compare_SameSuitNeitherTrump()    ,
            (true , false) => Greater                           ,
            (false, true ) => Less                              ,
            (false, false) => Equal                             ,
        }
    };

    if lhs == rhs
    {
        return Equal;
    }

    let lhsTrump = IsTrump(lhs, theTrumpSuit);
    let rhsTrump = IsTrump(rhs, theTrumpSuit);

    match (lhsTrump, rhsTrump)
    {
        (true , true ) => Compare_BothTrump()       ,
        (true , false) => Greater                   ,
        (false, true ) => Less                      ,
        (false, false) => Compare_NeitherTrump()    ,
    }
}

pub fn GetWinningCardIndex(someCards : &[Option<Card>], theLeadSuit : LeadSuit, theTrumpSuit : TrumpSuit) -> CardIndex
{
    if someCards.is_empty()
    {
        panic!("Can't get winning card from empty list!");
    }

    // NOTE: Can avoid collecting into vector by using iter::max_by when it becomes stable.
    let mut sortedCards : Vec<(CardIndex, Card)> = someCards.iter()
                                                            .enumerate()
                                                            .filter(|&(_, x)| x.is_some())
                                                            .map(|(i, x)| (i, x.unwrap()) )
                                                            .collect();

    sortedCards.sort_by(|a, b| CompareTwoCards(&a.1, &b.1, theLeadSuit, theTrumpSuit));

    // Sanity check.
    if sortedCards.len() >= 2
    {
        let       bestCard = sortedCards[sortedCards.len() - 1].1;
        let secondBestCard = sortedCards[sortedCards.len() - 2].1;

        let cmp = CompareTwoCards(&bestCard, &secondBestCard, theLeadSuit, theTrumpSuit);

        assert_eq!
        (
            cmp, ::std::cmp::Ordering::Greater,
            "Winning card is not actually winning! cards:{:?} sorted:{:?} lead:{:?} trump:{:?} best:{:?} second:{:?}",
            someCards,
            sortedCards,
            theLeadSuit,
            theTrumpSuit,
            bestCard,
            secondBestCard
        );
    }

    sortedCards.last().unwrap().0
}

pub fn DoesHandHaveSuitInIt(hand : &[Option<Card>], aSuit : CardSuit, anOptTrumpSuit : Option<TrumpSuit>) -> bool
{
    fn CheckSuit(aCard : &Card, aSuit : CardSuit, anOptTrumpSuit : Option<TrumpSuit>) -> bool
    {
        if let Some(aTrumpSuit) = anOptTrumpSuit
        {
            GetEffectiveSuit(&aCard, aTrumpSuit) == aSuit
        }
        else
        {
            aCard.0 == aSuit
        }
    }

    hand.iter().any(|anOptCard| anOptCard.map_or(false, |aCard| CheckSuit(&aCard, aSuit, anOptTrumpSuit)))
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn TestTeamsAndPartners()
    {
        for playerIndex in 0 .. 4
        {
            let partnerIndex = GetPartner(playerIndex);

            // Can't be your own partner.
            assert_ne!(playerIndex, partnerIndex);

            // Your partner's partner is you.
            assert_eq!(playerIndex, GetPartner(partnerIndex));

            // You and your partner are on the same team.
            assert_eq!(GetTeam(playerIndex), GetTeam(partnerIndex));
        }

        // Check explicit partner index.
        assert_eq!(GetPartner(0), 2);
        assert_eq!(GetPartner(1), 3);
        assert_eq!(GetPartner(2), 0);
        assert_eq!(GetPartner(3), 1);

        // Check explicit team index;
        assert_eq!(GetTeam(0), 0);
        assert_eq!(GetTeam(1), 1);
        assert_eq!(GetTeam(2), 0);
        assert_eq!(GetTeam(3), 1);

        // Check same team.
        assert_eq!(IsSameTeam(0, 0), true );
        assert_eq!(IsSameTeam(0, 1), false);
        assert_eq!(IsSameTeam(0, 2), true );
        assert_eq!(IsSameTeam(0, 3), false);

        assert_eq!(IsSameTeam(1, 0), false);
        assert_eq!(IsSameTeam(1, 1), true );
        assert_eq!(IsSameTeam(1, 2), false);
        assert_eq!(IsSameTeam(1, 3), true );

        assert_eq!(IsSameTeam(2, 0), true );
        assert_eq!(IsSameTeam(2, 1), false);
        assert_eq!(IsSameTeam(2, 2), true );
        assert_eq!(IsSameTeam(2, 3), false);

        assert_eq!(IsSameTeam(3, 0), false);
        assert_eq!(IsSameTeam(3, 1), true );
        assert_eq!(IsSameTeam(3, 2), false);
        assert_eq!(IsSameTeam(3, 3), true );
    }

    #[test]
    fn TestIsTrump()
    {
        use crate::card::*;
        use crate::card::CardSuit::*;

        let shuffledDeck = NewShuffledCardDeck_ThreadRNG();

        // General test to make sure we alwats have the right number of trump.
        for theTrumpSuit in CARD_SUIT_LIST.iter().map(|&aSuit| TrumpSuit(aSuit) )
        {
            let numTrump = shuffledDeck.iter().filter(|&aCard| IsTrump(aCard, theTrumpSuit)).count();

            assert_eq!(numTrump, 7);
        }

        // Specific tests.
        assert!( IsTrump(&CLUBS_J   , TrumpSuit(Clubs)) == true  );
        assert!( IsTrump(&SPADES_J  , TrumpSuit(Clubs)) == true  );
        assert!( IsTrump(&HEARTS_J  , TrumpSuit(Clubs)) == false );
        assert!( IsTrump(&DIAMONDS_J, TrumpSuit(Clubs)) == false );
    }

    #[test]
    fn TestCalculateScore()
    {
        // Going together.
        assert_eq!( CalculateScore(&[5, 0], 0, false), [2, 0] ); // Player 0
        assert_eq!( CalculateScore(&[4, 1], 0, false), [1, 0] );
        assert_eq!( CalculateScore(&[3, 2], 0, false), [1, 0] );
        assert_eq!( CalculateScore(&[2, 3], 0, false), [0, 2] );
        assert_eq!( CalculateScore(&[1, 4], 0, false), [0, 2] );
        assert_eq!( CalculateScore(&[0, 5], 0, false), [0, 2] );

        assert_eq!( CalculateScore(&[5, 0], 1, false), [2, 0] ); // Player 1
        assert_eq!( CalculateScore(&[4, 1], 1, false), [2, 0] );
        assert_eq!( CalculateScore(&[3, 2], 1, false), [2, 0] );
        assert_eq!( CalculateScore(&[2, 3], 1, false), [0, 1] );
        assert_eq!( CalculateScore(&[1, 4], 1, false), [0, 1] );
        assert_eq!( CalculateScore(&[0, 5], 1, false), [0, 2] );

        assert_eq!( CalculateScore(&[5, 0], 2, false), [2, 0] ); // Player 2
        assert_eq!( CalculateScore(&[4, 1], 2, false), [1, 0] );
        assert_eq!( CalculateScore(&[3, 2], 2, false), [1, 0] );
        assert_eq!( CalculateScore(&[2, 3], 2, false), [0, 2] );
        assert_eq!( CalculateScore(&[1, 4], 2, false), [0, 2] );
        assert_eq!( CalculateScore(&[0, 5], 2, false), [0, 2] );

        assert_eq!( CalculateScore(&[5, 0], 3, false), [2, 0] ); // Player 3
        assert_eq!( CalculateScore(&[4, 1], 3, false), [2, 0] );
        assert_eq!( CalculateScore(&[3, 2], 3, false), [2, 0] );
        assert_eq!( CalculateScore(&[2, 3], 3, false), [0, 1] );
        assert_eq!( CalculateScore(&[1, 4], 3, false), [0, 1] );
        assert_eq!( CalculateScore(&[0, 5], 3, false), [0, 2] );

        // Going alone.
        assert_eq!( CalculateScore(&[5, 0], 0, true ), [4, 0] ); // Player 0
        assert_eq!( CalculateScore(&[4, 1], 0, true ), [1, 0] );
        assert_eq!( CalculateScore(&[3, 2], 0, true ), [1, 0] );
        assert_eq!( CalculateScore(&[2, 3], 0, true ), [0, 2] );
        assert_eq!( CalculateScore(&[1, 4], 0, true ), [0, 2] );
        assert_eq!( CalculateScore(&[0, 5], 0, true ), [0, 2] );

        assert_eq!( CalculateScore(&[5, 0], 1, true ), [2, 0] ); // Player 1
        assert_eq!( CalculateScore(&[4, 1], 1, true ), [2, 0] );
        assert_eq!( CalculateScore(&[3, 2], 1, true ), [2, 0] );
        assert_eq!( CalculateScore(&[2, 3], 1, true ), [0, 1] );
        assert_eq!( CalculateScore(&[1, 4], 1, true ), [0, 1] );
        assert_eq!( CalculateScore(&[0, 5], 1, true ), [0, 4] );

        assert_eq!( CalculateScore(&[5, 0], 2, true ), [4, 0] ); // Player 2
        assert_eq!( CalculateScore(&[4, 1], 2, true ), [1, 0] );
        assert_eq!( CalculateScore(&[3, 2], 2, true ), [1, 0] );
        assert_eq!( CalculateScore(&[2, 3], 2, true ), [0, 2] );
        assert_eq!( CalculateScore(&[1, 4], 2, true ), [0, 2] );
        assert_eq!( CalculateScore(&[0, 5], 2, true ), [0, 2] );

        assert_eq!( CalculateScore(&[5, 0], 3, true ), [2, 0] ); // Player 3
        assert_eq!( CalculateScore(&[4, 1], 3, true ), [2, 0] );
        assert_eq!( CalculateScore(&[3, 2], 3, true ), [2, 0] );
        assert_eq!( CalculateScore(&[2, 3], 3, true ), [0, 1] );
        assert_eq!( CalculateScore(&[1, 4], 3, true ), [0, 1] );
        assert_eq!( CalculateScore(&[0, 5], 3, true ), [0, 4] );
    }

    #[test]
    fn TestGetPlayableCards()
    {
        use crate::card::*;
        use crate::card::CardSuit::*;

        // Playing the first card.
        assert_eq!( GetPlayableCards(&[Some(CLUBS_9), Some(DIAMONDS_9), Some(SPADES_9), Some(HEARTS_9), Some(CLUBS_K )], TrumpSuit(Clubs   ), None                    ), [true , true , true , true , true ] );
        assert_eq!( GetPlayableCards(&[Some(CLUBS_9), Some(DIAMONDS_9), Some(SPADES_9), Some(HEARTS_9), Some(CLUBS_K )], TrumpSuit(Diamonds), None                    ), [true , true , true , true , true ] );
        assert_eq!( GetPlayableCards(&[Some(CLUBS_9), Some(DIAMONDS_9), Some(SPADES_9), Some(HEARTS_9), Some(CLUBS_K )], TrumpSuit(Hearts  ), None                    ), [true , true , true , true , true ] );
        assert_eq!( GetPlayableCards(&[Some(CLUBS_9), Some(DIAMONDS_9), Some(SPADES_9), Some(HEARTS_9), Some(CLUBS_K )], TrumpSuit(Spades  ), None                    ), [true , true , true , true , true ] );

        // Playing after the first card.
        assert_eq!( GetPlayableCards(&[Some(CLUBS_9), Some(DIAMONDS_9), Some(SPADES_9), Some(HEARTS_9), Some(CLUBS_K )], TrumpSuit(Clubs   ), Some(LeadSuit(Diamonds))), [false, true , false, false, false] );
        assert_eq!( GetPlayableCards(&[Some(CLUBS_9), Some(DIAMONDS_9), Some(SPADES_9), Some(HEARTS_9), Some(CLUBS_K )], TrumpSuit(Diamonds), Some(LeadSuit(Hearts  ))), [false, false, false, true , false] );
        assert_eq!( GetPlayableCards(&[Some(CLUBS_9), Some(DIAMONDS_9), Some(SPADES_9), Some(HEARTS_9), Some(CLUBS_K )], TrumpSuit(Hearts  ), Some(LeadSuit(Spades  ))), [false, false, true , false, false] );
        assert_eq!( GetPlayableCards(&[Some(CLUBS_9), Some(DIAMONDS_9), Some(SPADES_9), Some(HEARTS_9), Some(CLUBS_K )], TrumpSuit(Spades  ), Some(LeadSuit(Clubs   ))), [true , false, false, false, true ] );

        // Testing bower suit logic.
        assert_eq!( GetPlayableCards(&[Some(CLUBS_J), Some(DIAMONDS_J), Some(SPADES_J), Some(HEARTS_J), Some(CLUBS_K )], TrumpSuit(Hearts  ), Some(LeadSuit(Diamonds))), [true , true , true , true , true ] );
        assert_eq!( GetPlayableCards(&[Some(CLUBS_J), Some(DIAMONDS_J), Some(SPADES_J), Some(HEARTS_J), Some(CLUBS_K )], TrumpSuit(Diamonds), Some(LeadSuit(Hearts  ))), [true , true , true , true , true ] );
        assert_eq!( GetPlayableCards(&[Some(CLUBS_J), Some(DIAMONDS_J), Some(SPADES_J), Some(HEARTS_J), Some(CLUBS_K )], TrumpSuit(Clubs   ), Some(LeadSuit(Spades  ))), [true , true , true , true , true ] );
        assert_eq!( GetPlayableCards(&[Some(CLUBS_J), Some(DIAMONDS_J), Some(SPADES_J), Some(HEARTS_J), Some(HEARTS_K)], TrumpSuit(Spades  ), Some(LeadSuit(Clubs   ))), [true , true , true , true , true ] );
    }

    #[test]
    fn TestCompareTwoCards()
    {
        use std::cmp::Ordering::*;

        use crate::card::*;
        use crate::card::CardSuit::*;

        let deck = NewUnshuffledCardDeck();

        // General checks.
        for c1 in deck.iter()
        {
            for c2 in deck.iter()
            {
                for aLeadSuit in CARD_SUIT_LIST.iter().map(|&aSuit| LeadSuit(aSuit) )
                {
                    for theTrumpSuit in CARD_SUIT_LIST.iter().map(|&aSuit| TrumpSuit(aSuit) )
                    {
                        let cmp1 = CompareTwoCards(c1, c2, aLeadSuit, theTrumpSuit);
                        let cmp2 = CompareTwoCards(c2, c1, aLeadSuit, theTrumpSuit);

                        // Check that comparisons are consistent.
                        assert_eq!(cmp1, cmp2.reverse());
                        assert_eq!(cmp2, cmp1.reverse());

                        if IsTrump(c1, theTrumpSuit) || IsTrump(c2, theTrumpSuit)
                        {
                            // Check trump always better than non trump.
                            if  IsTrump(c1, theTrumpSuit) && !IsTrump(c2, theTrumpSuit) { assert_eq!(cmp1, Greater); }
                            if !IsTrump(c1, theTrumpSuit) &&  IsTrump(c2, theTrumpSuit) { assert_eq!(cmp1, Less   ); }
                        }
                        else
                        {
                            // Check lead suit always better than non-lead suit.
                            if  IsLeadSuit(c1, theTrumpSuit, aLeadSuit) && !IsLeadSuit(c2, theTrumpSuit, aLeadSuit) { assert_eq!(cmp1, Greater); }
                            if !IsLeadSuit(c1, theTrumpSuit, aLeadSuit) &&  IsLeadSuit(c2, theTrumpSuit, aLeadSuit) { assert_eq!(cmp1, Less   ); }
                        }
                    }
                }
            }
        }

        // Specific checks.
        assert_eq!( CompareTwoCards(&CLUBS_J , &HEARTS_J, LeadSuit(Diamonds), TrumpSuit(Clubs )), Greater );
        assert_eq!( CompareTwoCards(&SPADES_J, &CLUBS_J , LeadSuit(Diamonds), TrumpSuit(Clubs )), Less    );
        assert_eq!( CompareTwoCards(&SPADES_J, &CLUBS_J , LeadSuit(Diamonds), TrumpSuit(Hearts)), Equal   );
    }

    #[test]
    fn TestGetWinningCardIndex()
    {
        use crate::card::*;
        use crate::card::CardSuit::*;

        assert_eq!( GetWinningCardIndex( &[Some(CLUBS_J ), Some(SPADES_J  ), Some(HEARTS_J ), Some(DIAMONDS_J)], LeadSuit(Clubs   ), TrumpSuit(Clubs   ) ), 0 );
        assert_eq!( GetWinningCardIndex( &[Some(CLUBS_J ), Some(SPADES_J  ), Some(HEARTS_J ), Some(DIAMONDS_J)], LeadSuit(Hearts  ), TrumpSuit(Clubs   ) ), 0 );
        assert_eq!( GetWinningCardIndex( &[Some(CLUBS_J ), Some(SPADES_J  ), Some(HEARTS_J ), Some(DIAMONDS_J)], LeadSuit(Diamonds), TrumpSuit(Clubs   ) ), 0 );
        assert_eq!( GetWinningCardIndex( &[Some(CLUBS_J ), Some(SPADES_J  ), Some(HEARTS_J ), Some(DIAMONDS_J)], LeadSuit(Spades  ), TrumpSuit(Clubs   ) ), 0 );

        assert_eq!( GetWinningCardIndex( &[Some(CLUBS_J ), Some(SPADES_J  ), Some(HEARTS_J ), Some(DIAMONDS_J)], LeadSuit(Clubs   ), TrumpSuit(Spades  ) ), 1 );
        assert_eq!( GetWinningCardIndex( &[Some(CLUBS_J ), Some(SPADES_J  ), Some(HEARTS_J ), Some(DIAMONDS_J)], LeadSuit(Hearts  ), TrumpSuit(Spades  ) ), 1 );
        assert_eq!( GetWinningCardIndex( &[Some(CLUBS_J ), Some(SPADES_J  ), Some(HEARTS_J ), Some(DIAMONDS_J)], LeadSuit(Diamonds), TrumpSuit(Spades  ) ), 1 );
        assert_eq!( GetWinningCardIndex( &[Some(CLUBS_J ), Some(SPADES_J  ), Some(HEARTS_J ), Some(DIAMONDS_J)], LeadSuit(Spades  ), TrumpSuit(Spades  ) ), 1 );

        assert_eq!( GetWinningCardIndex( &[Some(CLUBS_J ), Some(SPADES_J  ), Some(HEARTS_J ), Some(DIAMONDS_J)], LeadSuit(Clubs   ), TrumpSuit(Hearts  ) ), 2 );
        assert_eq!( GetWinningCardIndex( &[Some(CLUBS_J ), Some(SPADES_J  ), Some(HEARTS_J ), Some(DIAMONDS_J)], LeadSuit(Hearts  ), TrumpSuit(Hearts  ) ), 2 );
        assert_eq!( GetWinningCardIndex( &[Some(CLUBS_J ), Some(SPADES_J  ), Some(HEARTS_J ), Some(DIAMONDS_J)], LeadSuit(Diamonds), TrumpSuit(Hearts  ) ), 2 );
        assert_eq!( GetWinningCardIndex( &[Some(CLUBS_J ), Some(SPADES_J  ), Some(HEARTS_J ), Some(DIAMONDS_J)], LeadSuit(Spades  ), TrumpSuit(Hearts  ) ), 2 );

        assert_eq!( GetWinningCardIndex( &[Some(CLUBS_J ), Some(SPADES_J  ), Some(HEARTS_J ), Some(DIAMONDS_J)], LeadSuit(Clubs   ), TrumpSuit(Diamonds) ), 3 );
        assert_eq!( GetWinningCardIndex( &[Some(CLUBS_J ), Some(SPADES_J  ), Some(HEARTS_J ), Some(DIAMONDS_J)], LeadSuit(Hearts  ), TrumpSuit(Diamonds) ), 3 );
        assert_eq!( GetWinningCardIndex( &[Some(CLUBS_J ), Some(SPADES_J  ), Some(HEARTS_J ), Some(DIAMONDS_J)], LeadSuit(Diamonds), TrumpSuit(Diamonds) ), 3 );
        assert_eq!( GetWinningCardIndex( &[Some(CLUBS_J ), Some(SPADES_J  ), Some(HEARTS_J ), Some(DIAMONDS_J)], LeadSuit(Spades  ), TrumpSuit(Diamonds) ), 3 );

        assert_eq!( GetWinningCardIndex( &[Some(HEARTS_A), Some(DIAMONDS_A), Some(HEARTS_Q ), Some(CLUBS_10  )], LeadSuit(Hearts  ), TrumpSuit(Clubs   ) ), 3 );
    }

    #[test]
    fn TestDoesHandHaveSuitInIt()
    {
        unimplemented!();
    }
}
