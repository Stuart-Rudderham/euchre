#![allow(non_snake_case)]

use rand;

use crate::card::*;
use crate::player::*;
use crate::player::brain::IPlayerBrain;
use crate::logging::*;
use crate::euchre::utils::*;

fn ChooseTrumpFromKitty( players : &mut [Player; 4], startingPlayerIndex : PlayerIndex, cardFromKitty : &Card ) -> bool
{
    for i in 0..4
    {
        let index = (startingPlayerIndex + i) % 4;

        let player = &mut players[index];

        if player.ChooseTrumpFromKitty(cardFromKitty)
        {
            return true;
        }
    }

    false
}

fn ChooseTrumpFromHands( players : &mut [Player; 4], startingPlayerIndex : PlayerIndex, topCardOfKitty : &Card ) -> (PlayerIndex, TrumpSuit)
{
    for i in 0..4
    {
        let index = (startingPlayerIndex + i) % 4;

        let player = &mut players[index];

        let isLastPlayer = i == 3;

        if let Some(aSuit) = player.ChooseTrumpFromHand(isLastPlayer, topCardOfKitty)
        {
            return (index, aSuit);
        }
    }

    panic!("Logic error! No one chose trump!");
}

fn ChooseTrump( players : &mut [Player; 4], startingPlayerIndex : PlayerIndex, kitty : &mut CardKitty ) -> (PlayerIndex, TrumpSuit)
{
    let topCardOfKitty = &mut kitty[0];

    if ChooseTrumpFromKitty(players, startingPlayerIndex, topCardOfKitty )
    {
        let ret = (startingPlayerIndex, TrumpSuit(topCardOfKitty.0));

        let player = &mut players[startingPlayerIndex];

        player.SwapCardFromKitty(topCardOfKitty);

        ret
    }
    else
    {
        ChooseTrumpFromHands(players, startingPlayerIndex, topCardOfKitty)
    }
}

fn PlaySingleHand(somePlayers : &mut [Player; 4], theStartingPlayerIndex : PlayerIndex, theKitty : &CardKitty, theTrumpSuit : TrumpSuit, aGameStateRecorder : &mut GameStateRecorder ) -> PlayerIndex
{
    let mut cardsInPlay = [None, None, None, None];

    let mut leadSuit = None;

    aGameStateRecorder.hands.push( vec![] );

    for i in 0..4
    {
        let index = (theStartingPlayerIndex + i) % 4;

        let cardToPlay =
        {
            let player = &mut somePlayers[index];

            player.PlayCard( &cardsInPlay, theStartingPlayerIndex, leadSuit, theTrumpSuit )
        };

        cardsInPlay[index] = Some( cardToPlay );

        if i == 0
        {
            leadSuit = Some( LeadSuit(cardsInPlay[index].unwrap().0) );
        }

        let gameState = GameState
        {
            hand1 : somePlayers[0].hand.clone(),
            hand2 : somePlayers[1].hand.clone(),
            hand3 : somePlayers[2].hand.clone(),
            hand4 : somePlayers[3].hand.clone(),
            kitty : theKitty.clone(),

            cardsInPlay : cardsInPlay,

            score : [0, 0],

            trump : Some(theTrumpSuit),
        };

        aGameStateRecorder.hands.last_mut().unwrap().push(gameState);
    }

    GetWinningCardIndex(&cardsInPlay, leadSuit.unwrap(), theTrumpSuit)
}

fn PlayAllHands(somePlayers : &mut [Player; 4], mut theStartingPlayerIndex : PlayerIndex, theKitty : &CardKitty, theTrumpSuit : TrumpSuit, aGameStateRecorder : &mut GameStateRecorder ) -> [u8; 2]
{
    let mut handsWon = [0, 0];

    for _ in 0..5
    {
        let winningPlayerIndex = PlaySingleHand(somePlayers, theStartingPlayerIndex, theKitty, theTrumpSuit, aGameStateRecorder);

        let winningTeamIndex = GetTeam(winningPlayerIndex);

        handsWon[winningTeamIndex] += 1;

        theStartingPlayerIndex = winningPlayerIndex;
    }

    handsWon
}

pub fn PlaySingleGame<R : rand::Rng>(somePlayerBrains : &mut [&mut dyn IPlayerBrain; 4], aDealer : PlayerIndex, anRNG : &mut R, aGameStateRecorder : &mut GameStateRecorder ) -> [u8; 2]
{
    // Setup cards.
    let deck = NewShuffledCardDeck(anRNG);

    let (hand1, hand2, hand3, hand4, mut kitty) = DealHandsFromDeck(&deck);

    // Setup players.
    let (brain1, rest1) = somePlayerBrains.split_first_mut().unwrap(); // Need to split the array like this to make borrowck happy.
    let (brain2, rest2) = rest1           .split_first_mut().unwrap();
    let (brain3, rest3) = rest2           .split_first_mut().unwrap();
    let (brain4, _    ) = rest3           .split_first_mut().unwrap();

    let mut players =
    [
        Player::New( *brain1, hand1, 0 ),
        Player::New( *brain2, hand2, 1 ),
        Player::New( *brain3, hand3, 2 ),
        Player::New( *brain4, hand4, 3 ),
    ];

    // Logging.
    aGameStateRecorder.initial = Some
    (
        GameState
        {
            hand1 : players[0].hand.clone(),
            hand2 : players[1].hand.clone(),
            hand3 : players[2].hand.clone(),
            hand4 : players[3].hand.clone(),
            kitty : kitty.clone(),

            cardsInPlay : [None, None, None, None],

            score : [0, 0],

            trump : None,
        }
    );

    // Choose trump.
    let (playerThatCalledTrumpIndex, trumpSuit) = ChooseTrump(&mut players, aDealer, &mut kitty);

    // Play the game.
    let leadPlayer = (aDealer + 1) % 4;

    let handsWon = PlayAllHands(&mut players, leadPlayer, &kitty, trumpSuit, aGameStateRecorder);

    // Decide who won.
    CalculateScore(&handsWon, playerThatCalledTrumpIndex, false)
}

pub fn PlayEntireGame<R : rand::Rng>(somePlayerBrains : &mut [&mut dyn IPlayerBrain; 4], anRNG : &mut R, aStateRecorder : &mut GameStateRecorder) -> [u8; 2]
{
    let mut startingPlayerIndex = anRNG.gen_range(0, 4);

    let mut totalScore = [0, 0];

    let finalScore = 10;

    while (totalScore[0] < finalScore) && (totalScore[1] < finalScore)
    {
        let gameScore = PlaySingleGame(somePlayerBrains, startingPlayerIndex, anRNG, aStateRecorder );

        startingPlayerIndex = (startingPlayerIndex + 1) % 4;

        totalScore[0] += gameScore[0];
        totalScore[1] += gameScore[1];
    }

    let team0Won = totalScore[0] >= finalScore;
    let team1Won = totalScore[1] >= finalScore;

    // Make sure only 1 team won.
    assert_eq!( team0Won ^ team1Won, true );

    totalScore
}

#[test]
fn Test_SingleGame()
{
    use crate::player::brain::random::ThreadRngRandomPlayerBrain;

    use rand::Rng;

    let rng = &mut rand::thread_rng();

    let mut recorder = GameStateRecorder { initial : None, hands : vec![] };

    let startingPlayerIndex = rng.gen_range(0, 4);

    let mut brain1 = ThreadRngRandomPlayerBrain::New( rand::thread_rng() );
    let mut brain2 = ThreadRngRandomPlayerBrain::New( rand::thread_rng() );
    let mut brain3 = ThreadRngRandomPlayerBrain::New( rand::thread_rng() );
    let mut brain4 = ThreadRngRandomPlayerBrain::New( rand::thread_rng() );

    let mut playerBrains =
    [
        &mut brain1 as &mut dyn crate::player::brain::IPlayerBrain,
        &mut brain2 as &mut dyn crate::player::brain::IPlayerBrain,
        &mut brain3 as &mut dyn crate::player::brain::IPlayerBrain,
        &mut brain4 as &mut dyn crate::player::brain::IPlayerBrain,
    ];

    PlaySingleGame(&mut playerBrains, startingPlayerIndex, rng, &mut recorder);

    println!("");
    for s in recorder.Write()
    {
        println!("{}", s);
    }
    println!("");

    panic!("");
}

#[test]
fn Test_EntireGame()
{
    use crate::player::brain::random::ThreadRngRandomPlayerBrain;

    let rng = &mut rand::thread_rng();

    let mut recorder = GameStateRecorder { initial : None, hands : vec![] };

    let mut brain1 = ThreadRngRandomPlayerBrain::New( rand::thread_rng() );
    let mut brain2 = ThreadRngRandomPlayerBrain::New( rand::thread_rng() );
    let mut brain3 = ThreadRngRandomPlayerBrain::New( rand::thread_rng() );
    let mut brain4 = ThreadRngRandomPlayerBrain::New( rand::thread_rng() );

    let mut playerBrains =
    [
        &mut brain1 as &mut dyn crate::player::brain::IPlayerBrain,
        &mut brain2 as &mut dyn crate::player::brain::IPlayerBrain,
        &mut brain3 as &mut dyn crate::player::brain::IPlayerBrain,
        &mut brain4 as &mut dyn crate::player::brain::IPlayerBrain,
    ];

    println!( "{:?}", PlayEntireGame(&mut playerBrains, rng, &mut recorder) );
}
